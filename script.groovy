def buildJar() {
    echo "building the application..."
    bat 'mvn package'  
    // 'bat' for windows while 'sh' for linux/unix
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        bat 'docker build -t swastik18/demo-apps:jma-1.0 .'
        bat "echo $PASS | docker login -u $USER --password-stdin"
        bat 'docker push swastik18/demo-apps:jma-1.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this